#!/bin/bash
#
#SBATCH --gres=gpu:1
#SBATCH --job-name=FloydWarshall
#SBATCH --output=FloydWarshall-NCU
#SBATCH --account=ACD110018
#SBATCH --cpus-per-task=4
#SBATCH --nodes=1
#SBATCH --time=00:00:20

# export NCU="/home/samuel21119/usr/local/NVIDIA-Nsight-Compute-2022.1/ncu"

module load cuda/11.3
module load ncu

export filename="fw"

# Compile
nvcc $filename.cu -o $filename

export metrics="\
--section SpeedOfLight \
--section ComputeWorkloadAnalysis \
--section MemoryWorkloadAnalysis \
--section MemoryWorkloadAnalysis_Chart \
--section MemoryWorkloadAnalysis_Tables \
--section LaunchStats \
--section Occupancy \
--section SchedulerStats \
--section WarpStateStats
"
export kernel="--kernel-id ::gpu:1"
ncu -f --export report-$filename  $metrics $kernel ./$filename cases/c21.1 c21.1.out 
rm $filename
