#!/bin/bash
#
#SBATCH --gres=gpu:1
#SBATCH --job-name=FloydWarshall
#SBATCH --output=FloydWarshall-EXE
#SBATCH --account=ACD110018
#SBATCH --cpus-per-task=4
#SBATCH --nodes=1
#SBATCH --time=00:10:00

module load cuda/11.3

export filename="fw-cpu"

# Compile
nvcc $filename.cu -o $filename

time ./$filename cases/c21.1 $filename.out
cat $filename.out | md5sum