#include <fstream>
#include <cstdio>
#include <cstdlib>

inline void read_single(std::ifstream &in, int &x) {
    in.read(reinterpret_cast<char *>(&x), sizeof(x));
}
#define to1d(i, j, n) ((i) * (n) + j)

const int INF = ((1 << 30) - 1);
int n, *graph, *d_graph;
size_t read(char *);
void write(char *, size_t);

__global__ void gpu(int *g, int n, int k) {
    int id = blockIdx.x * blockDim.x + threadIdx.x;
    int j = id / n;
    int i = id % n;
    if (j >= n)
        return;
    g[to1d(i, j, n)] = min(g[to1d(i, j, n)], g[to1d(i, k, n)] + g[to1d(k, j, n)]);
}

int main(int argc, char* argv[]) {
    // sz = n * n * sizeof(int)
	size_t sz = read(argv[1]);

    // copy data from CPU to GPU:
    cudaMalloc(&d_graph, sz);
    cudaMemcpy(d_graph, graph, sz, cudaMemcpyHostToDevice);

    int nn = n * n;
    int blocksize = 1024;
    int blocks = (nn / blocksize) + (nn % blocksize != 0 ? 1 : 0);
    printf("n=%d, blocks=%d\n", n, blocks);
    for (int k = 0; k < n; k++) {
        gpu<<<blocks, blocksize>>>(d_graph, n, k);
    }

    cudaMemcpy(graph, d_graph, sz, cudaMemcpyDeviceToHost);

    write(argv[2], sz);
}

inline size_t read(char argv[]) {/*{{{*/
    int m;
    std::ifstream fin(argv, std::ios::in | std::ios::binary); 
    read_single(fin, n);
    read_single(fin, m);
    size_t sz = n * n * sizeof(int);
    cudaMallocHost(&graph, sz);
    // graph = (int*)malloc(sz);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            graph[to1d(i, j, n)] = INF;
        }
        graph[to1d(i, i, n)] = 0;
    }
    size_t sz2 = sizeof(int) * m * 3;
    int *input = (int*)malloc(sz2);
    fin.read((char*)input, sz2);
    fin.close();
    for (int i = 0; i < m; i++) {
        int &A = input[i * 3];
        int &B = input[i * 3 + 1];
        int &C = input[i * 3 + 2];
        graph[to1d(A, B, n)] = C;
    }
    return sz;
}
inline void write(char argv[], size_t sz) {
    std::ofstream fout(argv, std::ios::out | std::ios::binary);
    fout.write((char*)graph, sz);
    fout.close();
}/*}}}*/
