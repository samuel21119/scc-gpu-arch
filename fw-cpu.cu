#include <fstream>
#include <cstdio>
#include <cstdlib>

inline void read_single(std::ifstream &in, int &x) {
    in.read(reinterpret_cast<char *>(&x), sizeof(x));
}
#define to1d(i, j, n) ((i) * (n) + j)

const int INF = ((1 << 30) - 1);
int n, *graph, *d_graph;
size_t read(char *);
void write(char *, size_t);

// TODO: gpu kernel function
__global__ void gpu(int *graph, int n, int k) {
}

int main(int argc, char* argv[]) {
    // sz = n * n * sizeof(int)
	size_t sz = read(argv[1]);

    int nn = n * n;
    // copy data from CPU to GPU:
    // cudaMalloc(&d_graph, sz);
    // cudaMemcpy(d_graph, graph, sz, cudaMemcpyHostToDevice);

    for (int k = 0; k < n; k++) {
        // TODO: parallel the following 2 nest loops
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                // We convert a 2d array to 1d for easier GPU implementation
                // graph[i][j] = min(graph[i][j], graph[i][k] + graph[k][j]);
                graph[to1d(i, j, n)] = min(graph[to1d(i, j, n)], graph[to1d(i, k, n)] + graph[to1d(k, j, n)]);
            }
        }
    }

    // copy data from GPU back to CPU
    // cudaMemcpy(graph, d_graph, sz, cudaMemcpyDeviceToHost);

    write(argv[2], sz);
}

inline size_t read(char argv[]) {/*{{{*/
    int m;
    std::ifstream fin(argv, std::ios::in | std::ios::binary); 
    read_single(fin, n);
    read_single(fin, m);
    size_t sz = n * n * sizeof(int);
    cudaMallocHost(&graph, sz);
    // graph = (int*)malloc(sz);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            graph[to1d(i, j, n)] = INF;
        }
        graph[to1d(i, i, n)] = 0;
    }
    size_t sz2 = sizeof(int) * m * 3;
    int *input = (int*)malloc(sz2);
    fin.read((char*)input, sz2);
    fin.close();
    for (int i = 0; i < m; i++) {
        int &A = input[i * 3];
        int &B = input[i * 3 + 1];
        int &C = input[i * 3 + 2];
        graph[to1d(A, B, n)] = C;
    }
    return sz;
}
inline void write(char argv[], size_t sz) {
    std::ofstream fout(argv, std::ios::out | std::ios::binary);
    fout.write((char*)graph, sz);
    fout.close();
}/*}}}*/
