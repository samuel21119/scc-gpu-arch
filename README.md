# CUDA and Nsight Compute Lab

## File
- `ncu`: Nsight Compute module file
- `*-job.sh`: Slurm job file
- `fw-cpu.cu`: Floyd-Warshall CPU sequential code
- `fw.cu, fw2.cu`: Profiling target program